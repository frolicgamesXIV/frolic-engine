#pragma once

struct Entity {
	int id;
};

namespace EntitySystem {
	const int MAX_ENTITIES = 50;
	
	Entity g_entities[MAX_ENTITIES];

	void Initialize() {
		for (int i = 0; i < MAX_ENTITIES; ++i) {
			g_entities[i].id = i;
		}
	}

	Entity GetEntity(int id) {
		return g_entities[id];
	}
}