#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <chrono>

namespace Debug {
	void Log(std::string log) {
		std::ofstream logText("EngineLog.txt");

		std::cout << log << std::endl;
		
		auto systemTime = std::chrono::system_clock::now();
		//logText << log << " " << systemTime <<  "\n" << std::endl;
	}
}