#include <iostream>
#include "Debug.h"
#include "EntitySystem.h"

int main() {
	EntitySystem::Initialize();

	Entity e = EntitySystem::GetEntity(2);
	
	std::cout << "id test" << e.id << std::endl;
	
}